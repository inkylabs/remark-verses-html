import visit from 'unist-util-visit'

export default (opts) => {
  return (root, f) => {
    visit(root, 'verseref', (node, index, parent) => {
      node.type = 'span'
      if (node.attributes.class) {
        node.attributes.class.push('verseref')
      } else {
        node.attributes.class = ['verseref']
      }
      node.children = [{
        type: 'text',
        value: node.value,
        position: node.position
      }]
    })
  }
}
